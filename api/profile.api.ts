export interface ProfileApi {
    name: string;
    email: string;
    title: string;
    city: string;
    address: string;
    avatar: string;
    id: number;
    suitable: boolean;
}
